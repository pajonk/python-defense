'''
@author: FeniX
'''

from Engine.EnemyModel import EnemyModel
from random import random

class WaveModel(object):
    startTime = 0 #time in seconds after starting the game, when the wave should attack
    enemiesCount = 1
    enemies = None
    started = False
    waveNumber = -1

    def __init__(self, _startTime, _enemiesCount, _waveNumber):
        self.startTime = _startTime
        self.enemiesCount = _enemiesCount
        self.waveNumber = _waveNumber
        self.__prepareEnemies(_enemiesCount)


    def __prepareEnemies(self, _enemiesCount):
        self.enemies = []
        for x in xrange(0, _enemiesCount):
            enemy = EnemyModel(20, 1*self.waveNumber + _enemiesCount - x, 0, 0, self.waveNumber)
            enemy.nextPathPoint = 0
            self.enemies.append(enemy)