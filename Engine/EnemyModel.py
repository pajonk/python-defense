'''
@author: Piotrek
'''
import pygame
import os, sys

import pygame
from pygame.locals import *
def load_image(name, colorkey=None):
    fullname = os.path.join('Sprites', 'Enemies', 'Tank', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image:', name
        raise SystemExit, message
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()

class EnemyModel(pygame.sprite.Sprite):
    hitpoints = 50
    reward = 100
    positionX = 0
    positionY = 0
    speed = 10.0
    nextPathPoint = None
    waveNumber = 0
    degrees = 0
    currentFrame = 1
    delay = 10
    currentDelay = delay

    def __init__(self, _hitpoints, _speed, _positionX, _positionY, _waveNumber):
        self.hitpoints = _hitpoints #*10000
        self.speed = _speed*10
        self.positionX = _positionX
        self.positionY = _positionY
        self.waveNumber = _waveNumber

        pygame.sprite.Sprite.__init__(self)

        self.currentFrame = 0
        self.image, self.rect = load_image('L0.png', -1)
        self.rect.topleft = _positionX, _positionY

        center = self.rect.center
        self.image = pygame.transform.scale(self.image, (40, 40))
        self.rect = self.image.get_rect()
        self.rect.center = center


    def hit(self, damage):
        self.hitpoints -= damage
        return self.hitpoints

    def turn(self, degrees):
        # if degrees >= -22.5 and degrees < 22.5:
        #     self.image, self.rect = load_image('W.png', -1)
        # elif degrees >= 22.5 and degrees < 67.5:
        #     self.image, self.rect = load_image('NW.png', -1)
        # elif degrees >= 67.5 and degrees < 112.5:
        #     self.image, self.rect = load_image('N.png', -1)
        # elif degrees >= 112.5 and degrees < 157.5:
        #     self.image, self.rect = load_image('NE.png', -1)
        # elif (degrees >= 157.5 and degrees <= 180) or (degrees >= -180 and degrees < -157.5) :
        #     self.image, self.rect = load_image('E.png', -1)
        # elif degrees >= -157.5 and degrees < -112.5:
        #     self.image, self.rect = load_image('SE.png', -1)
        # elif degrees >= -112.5 and degrees < -67.5:
        #     self.image, self.rect = load_image('S.png', -1)
        # elif degrees >= -67.5 and degrees < -22.5:
        #     self.image, self.rect = load_image('SW.png', -1)
        
        self.degrees = -180-degrees


        if self.currentDelay == 0:
            self.currentFrame = (self.currentFrame + 1) % 2 
            self.currentDelay = self.delay
        else:
            self.currentDelay -= 1

        self.image, self.rect = load_image('L%d.png' % self.currentFrame, -1)
        self.rect.center = self.positionX, self.positionY
        center = self.rect.center
        self.image = pygame.transform.scale(self.image, (40, 40))

        if degrees < 90 and degrees > -90:
            self.image = pygame.transform.flip(self.image, False, True)
        self.image = pygame.transform.rotate(self.image, self.degrees)
        
        self.rect = self.image.get_rect()
        self.rect.center = center
        

