'''
@author: Piotrek
'''
from Engine.AmmoModel import AmmoModel


import pygame
import os, sys

import pygame
from pygame.locals import *
def load_image(name, colorkey=None):
    fullname = os.path.join('Sprites', 'Towers', 'Tower1', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image:', name
        raise SystemExit, message
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()


class TowerModel(pygame.sprite.Sprite):
    level = 1 #tower level
    fireRange = 100.0 
    speed = 10 #fire speed
    towerType = 1 #tower type for animation
    ammo = None #current ammo
    positionX = 0 #real position X
    positionY = 0 #real position Y
    sellCost = 0
    timeForFire = 0
    isShooting = False
    maxLevel = 5
    ammoSprites = pygame.sprite.RenderPlain()

    def __init__(self, towerType, level, fireRange, speed, ammo, positionX, positionY):
        self.towerType = towerType
        self.level = level
        self.fireRange = fireRange
        self.speed = speed
        self.ammo = ammo
        self.positionX = positionX
        self.positionY = positionY
        self.timeForFire = 1000/speed


        pygame.sprite.Sprite.__init__(self)

        
        self.image, self.rect = load_image('tower1@2x.png', -1)
        self.rect.center = positionX, positionY

        center = self.rect.center
        self.image = pygame.transform.scale(self.image, (40, 40))
        self.rect = self.image.get_rect()
        self.rect.center = center



    def fire(self, target):
        newAmmo = AmmoModel(1, self.ammo.damage);
        newAmmo.speed = self.ammo.speed;
        newAmmo.target = target
        newAmmo.positionX = self.positionX
        newAmmo.positionY = self.positionY
        newAmmo.source = self
        self.ammoSprites.add(newAmmo)

        return newAmmo
    def canBeUpgraded(self):
        if self.level < self.maxLevel:
            return True
        else:
            return False
    def upgrade(self):
        if self.canBeUpgraded():
            self.level += 1
            self.speed *= 1.2
            self.fireRange *= 2
            self.ammo.damage *= 1.5
            return True
        else:
            return False

    def turn(self, degrees):
        # if degrees >= -22.5 and degrees < 22.5:
        #     self.image, self.rect = load_image('W.png', -1)
        # elif degrees >= 22.5 and degrees < 67.5:
        #     self.image, self.rect = load_image('NW.png', -1)
        # elif degrees >= 67.5 and degrees < 112.5:
        #     self.image, self.rect = load_image('N.png', -1)
        # elif degrees >= 112.5 and degrees < 157.5:
        #     self.image, self.rect = load_image('NE.png', -1)
        # elif (degrees >= 157.5 and degrees <= 180) or (degrees >= -180 and degrees < -157.5) :
        #     self.image, self.rect = load_image('E.png', -1)
        # elif degrees >= -157.5 and degrees < -112.5:
        #     self.image, self.rect = load_image('SE.png', -1)
        # elif degrees >= -112.5 and degrees < -67.5:
        #     self.image, self.rect = load_image('S.png', -1)
        # elif degrees >= -67.5 and degrees < -22.5:
        #     self.image, self.rect = load_image('SW.png', -1)
        
        #if(degrees < 0):
        degrees = degrees

        self.image, self.rect = load_image('tower1@2x.png', -1)
        self.rect.center = self.positionX, self.positionY

        center = self.rect.center
        self.image = pygame.transform.scale(self.image, (40, 40))
        
        self.image = pygame.transform.rotate(self.image, -90)
        self.image = pygame.transform.rotate(self.image, -degrees)
        #self.image = pygame.transform.flip(self.image, False, True)

        self.rect = self.image.get_rect()
        self.rect.center = center

