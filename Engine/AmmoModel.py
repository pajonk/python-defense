'''
@author: Piotrek
'''
import pygame

class AmmoModel(pygame.sprite.Sprite):
    ammoType = 1 #ammo type for animation
    damage = 1.0 #damage per hit
    positionX = 0 #real position X
    positionY = 0 #real position Y
    target = None #target enemy
    speed = 3.0 #ammo flight speed per second
    source = None #tower that fired this piece of ammo
    currentFrame = 0
    delay = 1
    currentDelay = delay
    doUpdate = False
    destroy = False


    def __init__(self, ammoType, damage):
        self.ammoType = ammoType
        self.damage = damage


        pygame.sprite.Sprite.__init__(self)

        self.currentFrame = 0
        
        self.image = pygame.image.load('Sprites/Ammo/bullet.png')
        self.rect = self.image.get_rect()

        self.rect.center = self.positionX, self.positionY

        center = self.rect.center
        self.image = pygame.transform.scale(self.image, (10, 5))
        self.rect.center = center

    def hit(self):

        self.currentFrame = 0
        self.image = pygame.image.load('Sprites/Ammo/0.png')
        self.rect = self.image.get_rect()

        self.rect.center = self.positionX, self.positionY

        center = self.rect.center
        self.image = pygame.transform.scale(self.image, (40, 40))
        self.rect.center = center


        self.doUpdate = True
        #print self.rect

    def update(self):
        if self.doUpdate:
            if self.currentDelay == 0:
                self.currentFrame += 1
                if self.currentFrame == 4:
                    self.currentDelay = self.delay * 3
                else:
                    self.currentDelay = self.delay
            else:
                self.currentDelay -= 1

            if self.currentFrame > 4:
                self.source.ammoSprites.remove(self)
                self.destroy = True
                #del self
            else:
                self.image = pygame.image.load('Sprites/Ammo/%d.png' % self.currentFrame)
                self.rect = self.image.get_rect()

                self.rect.center = self.positionX, self.positionY

                center = self.rect.center
                self.image = pygame.transform.scale(self.image, (100, 100))
                self.rect.center = center



    def turn(self, degrees):
        
        self.degrees = -180-degrees

        self.image = pygame.image.load('Sprites/Ammo/bullet.png')
        self.rect = self.image.get_rect()

        self.rect.center = self.positionX, self.positionY
        center = self.rect.center
        self.image = pygame.transform.scale(self.image, (10, 5))

        if degrees < 90 and degrees > -90:
            self.image = pygame.transform.flip(self.image, False, True)
        self.image = pygame.transform.rotate(self.image, self.degrees)
        
        self.rect = self.image.get_rect()
        self.rect.center = center

